//import useEffect from React
//import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Row, Col, Card, Button } from 'react-bootstrap';


//useState Hook
	//Hook in React is a kind of tool
	//The useState hook allows creation and manipulation of states

	//States are a way for React to keep track of any value and then associate it with a component

	//When a state change, React re-render ONLY the specific component or part of the component that changed(not the entire page or components whose states have not changed)

	//syntax

	//const [state, setState] = useState(default state)

	//state - this is a special kind of variable (can be named anything) that React uses to render components when needed

	//state setter - state setters are the only way to chagne a state's value. By convention, they are named after the state

	//default state - initial value of the state on component mount

export default function CourseCard({courseProp}){

	//check to see if the data was successfully passed
	//console.log(courseProp);
	//every component receives information in a form of an object
	//console.log(typeof courseProp);
	//console.log(courseProp.name);

/*  const [count, setCount] = useState(0);
  const [seats, setSeats] = useState(10);
*/
  /*function enroll(){
    if(count<10){
      setCount(count + 1);
      setSeats(seats - 1);  
    }

    else{
      alert("Maximum Enrollees Reached!")
    }
    
  }*/


  /*useEffect(()=>{

  	if(seats === 0){
  		alert("Maximum Enrollees Reached!")
  	}
  })*/

  //deconstruct the course properties in their own variable name
  const {name, description, price, _id} = courseProp;


  return (
    <Row className="mt-3 mb-3">
      <Col> 
        <Card className="cardHighlight p-3">
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>Description</Card.Subtitle>
            <Card.Text>
              {description}
            </Card.Text>
            <Card.Subtitle>Price</Card.Subtitle>
            <Card.Text>
              Php {price}
            </Card.Text>
         {/*   <Card.Text>
              Enrollees: {count}
            </Card.Text>
            <Card.Text>
              Seats Available: {seats}
            </Card.Text>*/}
            <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  )
}
