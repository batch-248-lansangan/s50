
import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {

  const { user, setUser } = useContext(UserContext);


  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [isActive, setIsActive] = useState(false); // declare and initialize isActive state variable

  console.log(email);
  console.log(password1);

  function authenticate(e){

    
    e.preventDefault();

    fetch('http://localhost:4000/users/login', {
        method:'POST',
        headers:{
          'Content-Type':'application/json'
        },
        body: JSON.stringify({
          email:email,
          password:password1
        })

    })
    .then(res=>res.json())
    .then(data=>{
        console.log(data)

        if(typeof data.access !== "undefined"){
            localStorage.setItem('token',data.access)
            retrieveUserDetails(data.access)

            Swal.fire({
              title:"Login Successful!",
              icon:"success",
              text:"Welcome to Booking App of 248!"

            })

        }else{
            Swal.fire({
              title:"Authentication Unsuccessful!",
              icon:"error",
              text:"Check your credentials!"

            })
        }

    })

    const retrieveUserDetails = (token) =>{

        fetch('http://localhost:4000/user/details',{
          headers:{
            Authorization: `Bearer ${token}`
        
          }

        })
        .then(res=>res.json())
        .then(data=>{
              console.log(data);

              setUser({
                  id: data._id,
                  isAdmin: data._isAdmin
            })
        })
    }



    //comment out to allow the page to render automatically after the submit button was triggered

    //set the email of the authenticated user in the local storage
    /*
    Syntax:
        localStorage.setItem("propertyName, value")

    */

    //localStorage.setItem("email",email);


    //setUser({email: localStorage.getItem('email')})

    //clear input fields
    setEmail("");
    setPassword1("");

    //alert(`Welcome back, ${email}!`)

  };

  useEffect(() => {
    if (email !== "" && password1 !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password1]);

  return (

    (user.id !== null) ?
    <Navigate to="/courses"/>

    :

    <>

     <Row className="mt-3 mb-3">
      <h1>Login</h1>

      <Form onSubmit={(e)=>authenticate(e)}>

        <Form.Group controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={password1}
            onChange={(e) => setPassword1(e.target.value)}
            required
            style={{ marginBottom: "10px" }}
          />
        </Form.Group>

        {isActive ?
          <Button variant="success" type="submit">
            Login
          </Button>
          :
          <Button variant="success" type="submit" disabled>
            Login
          </Button>
        }

      </Form>
      </Row>
     
    </>
  );
}