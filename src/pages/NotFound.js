/*
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';

export default function NotFound() {
  return (
    <div style={{ textAlign: 'left', marginTop: '100px' }}>
      <h1>404 Error:Page Not Found</h1>
      <p>The requested page does not exist. Please check the URL and try again.</p>
      <Button variant="primary" as={Link} to="/">Back to Home</Button>
    </div>
  );
}

*/

import Banner from '../components/Banner';


export default function Error(){

	const data = {

		title: "404 - Page Not Found",
		content: "The page you are looking for cannot be found",
		destination: "/",
		label: "Back to Home"

	}

	return (

		<Banner data={data}/>

	)

}
